//
//  IZAppDelegate.h
//  InboxZero
//
//  Created by Rene Cacheaux on 2/22/13.
//  Copyright (c) 2013 RCach Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@class IZViewController;

@interface IZAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) IZViewController *viewController;

@end
