//
//  main.m
//  InboxZero
//
//  Created by Rene Cacheaux on 2/22/13.
//  Copyright (c) 2013 RCach Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "IZAppDelegate.h"

int main(int argc, char *argv[])
{
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([IZAppDelegate class]));
  }
}
